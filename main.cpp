#include <iostream>
#include <cctype>
#include <cstring>
#include <iomanip>
using namespace std;

// Christine Nguyen
// CS162, Program 2, 02/09/2023
// This is a message shortener program. This program will get the user's word limit and
// message. As long as the message the user enters is below the max size of characters 250.
// It will then calculate how many words the message if over the user's word limit, and will shorten
// the message by removing words "a" and "the" and will replace words "and" with "-". This program
// will also implement my own rule, replacing the words "cannot" to "cant" and "is not" to "isnt".
// The user will input their message into an original array and then that array will copy the the wanted
// characters in a second array, and the third final array will print out the revised message.

const int SIZE{250};		// the max size of array

void printWelcome();
void getMessage(char message[], int user_size);
int calcSizeOver(int words, int user_size, int & over_under);
int getUserSize(int & user_size);
int getWords(char message[], int length, int words);
void revisedMessage(char message[], char message2[], int length, int j);
void getCapitalize(char message2[], int length2);
void newRule(char message2[], int length2, int j, int & counter);
bool again();

int main()
{
	char message[SIZE];	// the array of characters in the message
	char message2[SIZE];	// the revised message
	//char message3[SIZE];	// the final revised message getting the cannot and is not out
	int over_under {0};	// how much words are over or under
	int length {0};		// holds the size of the message
	int user_size {0};	// the limit of words the user's declares
	int words {1};		// number of words from a characters in array
	int i {0};		// index; for stepping through array
	int j {0};		// temp index for i while stepping through array
	int length2 {0};	// new length of message 2
	int counter{0};		// used to count how much extra index we have left after replacing words
	int length3{0};		// the length of message2 - the counter
	char choice;		// the user's choice if they want to rerun program

	printWelcome();
	do
	{
		user_size = getUserSize(user_size);
		getMessage(message, user_size);
		length = strlen(message);
		while(strlen(message) >= SIZE - 1)
		{
			cout << "\nMessage is too large. System cannot process.\n" << endl;
			message[0] = '\n';
			user_size = getUserSize(user_size);
			getMessage(message, user_size);
			length = strlen(message);
		}
		words = getWords(message, length, words);
		calcSizeOver( words, user_size, over_under);
		if (words > user_size)
		{
			revisedMessage(message, message2, length, j);
			length2 = strlen(message2);
			newRule(message2, length2, j, counter);
			getCapitalize(message2, length2);

			length3 = (strlen(message2) - counter);
			char * message3 = new char [length3 + 1];
			strncpy(message3, message2, length3);
			cout << "Revised Message: \n" << message3 << "\n" << endl;
			for(int i {0}; i < length; ++i)
			{
				message2[i] = ' ';
			}
			words = 1;	// we initialize it back to 1 to account for that first word
		}
	} while (again());
	return 0;
}


// Prints the welcome message and explains for the program will work
void printWelcome()
{
	cout << "Welcome to the paragraph shortening program!\n"
	     << "\nThis program will shorten your paragraph if it is over the word limit "
	     << "of your choosing.\n250 characters is the maximum amount of characters this program can process."
	     << "When your message is over the word limit.\nThe program will "
	     << "remove words such as: a, the, and. Words: cannot → can’t, is not → isn’t."
	     << endl;
	return;
}


// Will get the user's word limit for message
int getUserSize(int & user_size)
{
	cout << setfill('_') << setw(50) << endl;
	cout << "\nEnter word limit: ";
	cin >> user_size;
	cin.ignore(100, '\n');
	return user_size;
}


// Gets the user's message and echos it back
void getMessage(char message[], int user_size)
{
	cout << "\nType in message (" << user_size << " word limit): ";
	cin.get(message, SIZE, '\n');
	cin.ignore(100, '\n');
	cout << "\nOriginal Version: \n" << message << endl;
	return;
}


// Calculates if user is over or under the word limit
int calcSizeOver(int words, int user_size,int & over_under)
{
	over_under = words - user_size;
	if (over_under > 0)
	{
		cout << "\nYour message is " << over_under << " words over the "
		     << user_size <<" word limit\n" << endl;
	}
	else
	{
    		cout << "\nYour message is within the "<< user_size << " word limit, "
         	     << "no need to be shortened.\n" << endl;
 	}
	return over_under;
}


// Counts how many words are in an array of characters by counting spaces
int getWords(char message[], int length, int words)
{
	for(int i {0}; i < length; ++i)
	{
		if (message[i] == ' ' || message[i] == '\n')
		{
			words++;
		}
	}
	return words;
}


// Saves the new revised message in new array. Removes "a" and "there, and replaces "and" with "-"
void revisedMessage(char message[], char message2[], int length, int j)
{
	for(int i {0}; i < length; ++i)
	{
		if(toupper(message[i]) == 'T' && toupper(message[i+1]) == 'H' && toupper(message[i+2]) == 'E' 
		   && message[i+3] ==' ')
		{
			i += 3;		//3 is the length of char that we have to "delete", if message is [i + #] i+= #
			message2[j] = message[i];
		}
		else if(toupper(message[i]) == 'A' && message[i+1] ==' ')
		{
			i += 1;
			message2[j] = message[i];
		}
		else if(toupper(message[i]) == 'A' && toupper(message[i+1]) == 'N' && toupper(message[i+2]) == 'D' && message[i+3] ==' ' 
			|| toupper(message[i]) == 'A' && toupper(message[i+1]) == 'N' && toupper(message[i+2]) == 'D' && message[i+3] ==',' 
			|| toupper(message[i]) == ',' && toupper(message[i+1]) == 'A' && toupper(message[i+2]) == 'N' && message[i+3] =='D')
		{
			i += 3;
			message2[j-1] = '-';		// subtract 1 to go back space and adda dash
		}
		else
		{
			message2[j] = message[i];
			++j; 		// have to increment j since in the for loop we increment i
		}
	}
	return;
}


// Capitalize after punctuations
void getCapitalize(char message2[], int length2)
{
	for(int i  {0}; i < length2; ++i)
	{
		message2[0] = toupper(message2[0]);
		if( message2[i]=='.' && message2[i+1] == ' ' || message2[i]=='?' && message2[i+1] == ' ' || message2[i]=='!' && message2[i+1] == ' ')
          	{
            		i += 2;		// we meed to get the char after the space toupper, which is 2
          		message2[i] = toupper(message2[i]);
          	}
		else
                {
                        message2[i] = message2[i];
        	}
          }
	return;
}


// Replaces cannot and is not in message
void newRule(char message2[], int length2, int j, int & counter)
{
	for(int i {0}; i < length2; ++i)
	{
		if(i + 5 <length2)
		{
			if (toupper(message2[i]) == 'C' && toupper(message2[i+1]) == 'A' && toupper(message2[i+2]) == 'N' 
			    && toupper(message2[i+3]) == 'N' && toupper(message2[i+4]) == 'O' && toupper(message2[i+5]) == 'T')
			{
				i += 5;
        			message2[j] = 'c';
        			message2[j+1] = 'a';
        			message2[j+2] = 'n';
        			message2[j+3] = 't';
        			j +=4;
        			counter += 2;		// add 2 since cannot has a 2 char size difference in cant.
			}
			else if (toupper(message2[i]) == 'I' && toupper(message2[i+1]) == 'S' && toupper(message2[i+2]) == ' ' 
				 && toupper(message2[i+3]) == 'N' && toupper(message2[i+4]) == 'O' && toupper(message2[i+5]) == 'T')
			{
				i += 5;
        			message2[j] = 'i';
        			message2[j+1] = 's';
        			message2[j+2] = 'n';
        			message2[j+3] = 't';
        			j +=4;
        			counter +=2;	// add 2 since is not and is not has a 2 char size difference.
			}
			else
			{
				message2[j] = message2[i];
        			++j;
			}
		}
		else
		{
			message2[j] = message2[i];
        		++j;
		}
	}
	message2[j] = '\n';
	return;
}


// Get user choice if they want to rerun program
bool again()
{
	char choice {'N'};
	cout << "Would you like to shorten another message? (Y/N) : ";
	cin >> choice;
	cin.ignore(100, '\n');
	if (toupper(choice) == 'Y')
	{
		return true;
	}
	return false;
}
